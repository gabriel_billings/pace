
clear
%change to output file from R script as needed
fname='/Users/gbillings/Dropbox/WaldorScripts/out.txt';

fid=fopen(fname);
fgets(fid);
C=textscan(fid,['%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %s %s %s %f ',...
    '%f %f %f %d %f %f %f %f %f %f %f %f\n'], 'Delimiter','|');
fclose(fid);

tags=C{1};
FC=cat(2,C{2:7})';
FC_fit=cat(2,C{8:13})';
slope=C{14};
intercept=C{15};
annotation=C{17};
gene=C{18};
slopeCI=[C{19},C{20}];
interceptCI=[C{21},C{22}];
model=C{23};
RSS=C{24};
chi=C{25};
sd=C{26};

fitCoeffs=cat(2,C{27:29})';
p=C{30};
slopeLin=C{31};
fitCoeffsNorm=fitCoeffs; %zscore(fitCoeffs(1:3,:), 0,2);
gene(find(strcmp(gene,'NA')))=tags(find(strcmp(gene,'NA')));

for i=1:numel(gene)
    g=gene{i};
    g(find(g=='"'))=[];
    gene{i}=g;
end



for i=1:numel(tags)
    tag=tags{i};
    num=str2num(tag(end-5:end-1));
    if num>=4070 && num<=4235 
        t3(i)=1;
    else
        t3(i)=0;
    end
    
    if num>=11235 && num<=11310
        t6(i)=1;
    else
        t6(i)=0;
    end
end
secretion=t3 | t6 ;

%Restrict attention to IVD genes
cut=slopeCI(:,1)<0;
%Linkage/distance methods for clustering
linkMethod='average';
dist='mahalanobis';
numSec=[];


%%
scatter(fitCoeffs(1,cut),fitCoeffs(2,cut),50,t3(cut),'filled')
xlabel('Intercept (log2 fold change)');
ylabel('Slope (log2 fold change/day)');
print('PACE/scatterCoeffs.pdf','-dpdf')

%% Deal with genes without names

g2=gene;
noname=strcmp(gene,'0');
t2=tags;



for i=1:numel(tags)
    t=tags{i};
    t(find(t=='"'))=[];
    t2{i}=t;
end
g2(noname)=t2(noname);
geneCut=g2(cut);
%% Create dendrogram
close force all

d=pdist(fitCoeffsNorm(:,cut)',dist);
colormap jet
tree=linkage(d,linkMethod);
%Following lines show distance matrix
%imagesc(squareform(d))
%title(cophenet(tree,d))
l=arrayfun(@num2str,secretion(cut),'uniformoutput',false);
order=optimalleaforder(tree,d,'criteria','group','transformation','inverse');
order=flip(flip(order));
%user-determined threshold
thresh=2.25
[h,t]=dendrogram(tree,0,'colorthreshold',thresh,'reorder',order,'orientation','top','labels',geneCut);




print('PACE/dendrogramCut.pdf','-dpdf');

%% Color dendrogram appropriately
c=clusterdata(fitCoeffsNorm(:,cut)','cutoff',thresh,'criterion','distance','linkage',linkMethod,'distance',dist)
colors=reshape([h.Color],[3,numel(h)])';
[colors,~,colorInds]=unique(colors,'rows');

%pretty colormap, from colorbrewer2.org
cmap=[0,0,0;228,26,28;55,126,184;77,175,74;152,78,163;255,127,0;255,255,51];
%cmap=[0,0,0;166,206,227;31,120,180;178,223,138;51,160,44]
%cmap=[0,0,0;166,206,227;31,120,180;178,223,138;51,160,44;251,154,153;227,26,28;253,191,111;255,127,0;202,178,214;106,61,154]
cmap=cmap/255;
%can permute map for aesthetics
%perm=[   6     3     9     5    10     2     7     4     1     8];
%cmap(2:end,:)=cmap(perm+1,:);

for i=1:numel(h)
    set(h(i),'Color',cmap(colorInds(i),:),'linewidth',1.5);
end
geneOrdered=geneCut(order);

        
        
    

dS=squareform(d);
hold on
a=double(secretion(cut));
a=a(order);
hold on
t3cut=t3(cut);
t3cut=t3cut(order);
for x=find(a)
    if t3cut(x)
    plot([0.1,0.1],[x-0.5,x+0.5],'g--','linewidth',5);
    else
            plot([0.1,0.1],[x-0.5,x+0.5],'b--','linewidth',5);
    end

end
hold off
genesOrdered=flip(flip(geneCut(order)));
ax=gca;
t3Cluster=genesOrdered(33:66);
set(gca,'ticklength',[0,0])

geneToOrigInd=@(x) find(strcmp(x,geneCut));
orderedIndToGene=@(x) genesOrdered(x);
geneColor=[];
for i=1:numel(h)
    x=h(i).XData;
    y=h(i).YData;
    color=colorInds(i)
    
    %2 points at x=0 means terminal U
    if any(x==0)
     ys=y(x==0);
     for j=1:numel(ys)
           geneName=orderedIndToGene(ys(j));
           geneInd=geneToOrigInd(geneName);
           geneColor(geneInd)=color;
     end
           
%      disp(genesOrdered(numel(geneCut)+1-ys));
%      set(h(i),'LineWidth',4);
%      ax=axis(gca);
%      axis([min(x)-1,max(x)+1,min(y)-5,max(y)+5]);
%      pause;
%      set(h(i),'LineWidth',1.5);
%      axis(ax);
    end
end
        

set(gca,'yticklabel',[]);



print('PACE/dendrogram.pdf','-dpdf');
%% Dendrogrames for each cluster
for k=1:max(c)
    if sum(c==k)>1
close force all
figure
coeffsCut=fitCoeffsNorm(:,cut);
d2=squareform(d);
d2=squareform(d2(c==k,c==k));

tree2=linkage(d2,linkMethod);
[~,I]=sort(order);
[~,I2]=sort(I(c==k));

%order2=(optimalleaforder(tree2,d2,'criteria','group','transformation','inverse'));
[h,t]=dendrogram(tree2,0,'reorder',I2,'orientation','left','labels',geneCut(c==k));
set(gca,'xtick',[])
set(gca,'ticklength',[0,0]);
set(gca,'ticklabelinterpreter','none')
cmap(unique(geneColor(c==k)),:)
for i=1:numel(h)
    
    set(h,'color',cmap(unique(geneColor(c==k)),:),'linewidth',1.5);
end
a=double(secretion(cut));
a=a(c==k);
t=t3(cut);
t=t(c==k);
a=a(I2);
t=t(I2);
hold on
for x=find(a)
    if t(x)
    plot([0.1,0.1],[x-0.5,x+0.5],'g','linewidth',5);
    else
            plot([0.1,0.1],[x-0.5,x+0.5],'b','linewidth',5);
    end

end
hold off
ax=axis(gca);
ax(1)=0;
axis(ax);
print(['PACE/dendrogram_cluster',num2str(k),'.pdf'],'-dpdf')
 
    end
end
%% Clustered distance matrix
imagesc(dS(order,order))
geneSub=gene(cut);
set(gca,'XTick',1:size(dS),'XTickLabel',geneSub(order),'XTickLabelRotation',90)
set(gca,'YTick',1:size(dS),'YTickLabel',geneSub(order))




%%


%% Plot curves for each cluster
FC_cut=FC(:,cut);
fitCut=FC_fit(:,cut);
fitCoeffsCut=fitCoeffs(:,cut);
i=1
t=[1,2,5,8,11,14]
close force all
figure

for i=1:max(c)
    inds=find(c==i);
    hold off
    for j=1:numel(inds)
        k=inds(j);
        if t3C(k)
            colorChar=cmap(4,:);
        elseif t6C(k)
            colorChar=cmap(3,:);
        else
            colorChar=cmap(1,:);
        end
        plot(t,FC_cut(:,k),'linewidth',1,'Color',colorChar);
        hold on
        plot(t,fitCut(:,k),'linewidth',1,'linestyle','--','color',colorChar);
    end
    %plot(t,FC_cut(:,c==i),'linewidth',3)
    g3=g2(cut);
    clusterMean(:,i)=mean(FC_cut(:,c==i),2);
    clusterSTD(:,i)=std(FC_cut(:,c==i),[],2);
    meanCoeffs(:,i)=mean(fitCoeffsCut(:,c==i),2);
    meanCurve(:,i)=meanCoeffs(1,i)+meanCoeffs(2,i).*t'+meanCoeffs(3,i).*(t').^2;
    
    plot(t,clusterMean(:,i),'color',cmap(2,:),'linewidth',4);
    %plot(t,fitCut(:,c==i),'linewidth',0.5);
    
    axis([0,14,-8,2])
    xlabel('Days post infection');
    ylabel('Log2 fold change relative to input');
    print(['PACE/cluster_',num2str(i),'_withFit.pdf'],'-dpdf')
end

%%
scatter(fitCoeffsNorm(1,cut),fitCoeffsNorm(2,cut),60,c,'filled'); axis equal

%%
t=[1,2,5,8,11,14]
hold off
for i=1:max(c)
    if sum(c==i)>1
    curColor=geneColor(c==i);
    if numel(unique(curColor))~=1
        error('color problem');
    end
    c2=cmap(unique(curColor),:);
    plot(t,clusterMean(:,i),'linewidth',2,'color',c2);
    %plot(t,meanCurve(:,i),'--','linewidth',1,'color',c2);
    hold on
    end
end

%plot(t,clusterMean,'k--','linewidth',2);
%plot(t,FC_cut(:,c==1),'k--')
hold off
axis([0,14,-7,1])
xlabel('Days post infection');
ylabel('Log2(FC)');
legend(arrayfun(@(x) ['Cluster ',num2str(x)],1:max(c),'uniformoutput',false))
print('PACE/clusters_all2.pdf','-dpdf');
%%
t3C=logical(t3(cut));
t6C=logical(t6(cut));
f=fitCoeffs(:,cut);
cut1=~(t3C | t6C);
hold off
colormap(cmap)
% can plot t3/t6 with different sy
scatter(f(1,cut1),f(2,cut1),50,cmap(geneColor(cut1),:),'filled');
hold on
cut1=t3C
scatter(f(1,cut1),f(2,cut1),50,cmap(geneColor(cut1),:),'filled');
cut1=t6C
scatter(f(1,cut1),f(2,cut1),50,cmap(geneColor(cut1),:),'filled');
hold off
xlabel('Intercept (log2 fold change)');
ylabel('Slope (log2 fold change/day)');
print('PACE/scatterCluster.pdf','-dpdf')

%%
%secretionCenter=mean(fitCoeffsNorm(:,secretion & cut')')
%secretionStd=(std(fitCoeffsNorm(:,secretion & cut')'))

vaccineList={'aroC','pabA','trpD','esrB','ETAE_RS10450'};
colors=zeros(size(g2));

for i=1:numel(vaccineList)
    vaccineInds(i)=find(strcmp(vaccineList(i),g2));
    colors(vaccineInds(i))=i;
end

colors=colors(cut);
colors(c==2 & colors==0)=numel(vaccineList)+1;

aroC=find(cellfun(@numel,strfind(geneCut,'aroC')));

distances=squareform(pdist(fitCoeffsNorm(:,cut)',dist));

d_aroC=distances(aroC,:);


%dSecretion=sqrt(sum((fitCoeffsNorm'-repmat(secretionCenter,[size(fitCoeffs,2),1])).^2,2))
%d_aroC=sqrt(sum((fitCoeffsNorm'-repmat(aroCCoeffs',[size(fitCoeffs,2),1])).^2,2));






%scatter(fitCoeffs(1,cut),fitCoeffs(2,cut),50, d_aroC(cut),'filled')

outbreakIndex=mean(FC(4:end,:))
colormap(cmap)

scatter(outbreakIndex(cut),(d_aroC()),50,colors,'filled');
xlabel('Outbreak Index (log2(FC))');
ylabel('Distance from aroC profile (SD)');
print('PACE/VaccineSelection.pdf','-dpdf')
aroCut=d_aroC;
hold off;
nhist(d_aroC(c==2),20);

vaccineList=outbreakIndex(cut)<-3 & d_aroC<2 ;
%% Output to CSV
tagCut=tags(cut);
geneCut=gene(cut);
annotationCut=annotation(cut);
slopeCut=slope(cut);
fid=fopen('PACE/vaccineList.csv','w')
fprintf(fid, 'Tag, Gene, Annotation, Slope(log2FC/day), Cluster, AroC_Distance, Outbreak_Index\n'); 
outbreakCut=outbreakIndex(cut)
for i=find(vaccineList)
    a=annotationCut{i};
    a(a==',')=';';
    g=geneCut{i};
    g(g==',')=';';
    fprintf(fid,'%s, %s, %s, %f, %i,%f,%f \n', tagCut{i}, g, a,slopeCut(i),c(i),aroCut(i),outbreakCut(i));
end
fclose(fid);
%%

c2=c;
c2(c==1)=3;
c2(c==2)=2;
c2(c==3)=0;
c2(c==4)=0;
c2(c==5)=4;
c2(c==6)=1;
c2(c==7)=0;
tagCut=tags(cut);
geneCut=gene(cut);
annotationCut=annotation(cut);
slopeCut=slope(cut);
modelCut=model(cut);
interceptCut=intercept(cut)
fid=fopen('PACE/IVA.csv','w')
fprintf(fid, 'Tag, Gene, Annotation, Slope(log2FC/day), Cluster, AroC_Distance, Outbreak_Index, Intercept(log2FC), Model order\n'); 
outbreakCut=outbreakIndex(cut)
for i=1:numel(c)
    a=annotationCut{i};
    a(a==',')=';';
    g=geneCut{i};
    g(g==',')=';';
    fprintf(fid,'%s, %s, %s, %f, %i, %f, %f, %f, %i \n', tagCut{i}, g, a,slopeCut(i),c2(i),aroCut(i),outbreakCut(i),interceptCut(i),modelCut(i));
end
fclose(fid);

%%
for i=1:max(c)
fid2=fopen(['PACE/cluster',num2str(i),'.csv'],'w');
fprintf(fid, 'Tag, Gene, Annotation, Slope(log2FC/day), Cluster, AroC_Distance, Outbreak_Index\n'); 
c1=find(c==i);
for j=1:numel(c1)
    i=c1(j);
    disp(i);
    a=annotationCut{i};
    a(a==',')=';';
    g=geneCut{i};
    g(g==',')=';';
    fprintf(fid,'%s, %s, %s, %f, %i,%f,%f \n', tagCut{i}, g, a,slopeCut(i),c(i),aroCut(i),outbreakCut(i));

end
fclose(fid2);
end
%%
%bootstrapping CI for median
N=10000;
s=slopeCut(c==1);
boot=median(reshape(randsample(s,N*numel(s),true),numel(s),N));
CI=quantile(boot,[.025,0.975]);

